#include <sys/types.h>
#include <math.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <pwd.h>
#include <time.h>
#include <sys/wait.h>
#include <json-c/json.h>
#include <stdbool.h>
#include <dirent.h> 

char *linkdatabase[] = {"https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"};
char *dir[] = {"characters", "weapons"};
char *name[] = {"characters.zip", "weapons.zip"};
char charname[64][64];
char weaponname[256][64];
 int k1 = 0, k2 = 0;

bool isDone = false;

void forker(int total, int temp, int nprocesses){
    pid_t pid = fork();
    int status_pid;
    if(pid < 0){
        exit(EXIT_FAILURE);
    }
    if(pid == 0){
        if(nprocesses == temp){
        }
        else{
            forker(total - 90, temp+1, nprocesses);
        }
        char temp_path[1024];
        strcat(temp_path, "/home/januarevan/sisopmodul2/gacha_gacha/");
        strcat(temp_path, "total_gacha_");
        char temps[1024] = "";
        sprintf(temps, "%d", total);
        strcat(temp_path, temps);
        char *argv[] = {"mkdir", "-p", temp_path, NULL};
        execv("/usr/bin/mkdir", argv);
    }
    else{
        while((wait(&status_pid)) > 0);
        return;
    }
}

void gacha1(){
    pid_t gacha = fork();
    int status_gacha;
    if(gacha < 0){
        exit(EXIT_FAILURE);
    }
    if(gacha == 0){
        pid_t f1 = fork();
        int status_f1;
        if(f1 < 0){
            exit(EXIT_FAILURE);
        }
        if(f1 == 0){ //download 2 file
            pid_t f2 = fork();
            int status_f2;
            if(f2 < 0){
                exit(EXIT_FAILURE);
            }
            if(f2 == 0){
                pid_t f3 = fork();
                int status_f3;
                if(f3 < 0){
                    exit(EXIT_FAILURE);
                }
                if(f3 == 0){
                    char *argv[] = {"wget", "-q", "--no-check-certificate", linkdatabase[0], "-O", name[0], NULL};
                    execv("/usr/bin/wget", argv);
                }
                else{
                    while((wait(&status_f3)) > 0);
                    char *argv[] = {"unzip", "-qq", name[0], NULL};
                    execv("/usr/bin/unzip", argv);
                }
            }
            else{ //download weapon
                while((wait(&status_f2)) > 0);
                pid_t f4 = fork();
                int status_f4;
                if(f4 < 0){
                    exit(EXIT_FAILURE);
                }
                if(f4 == 0){
                    char *argv[] = {"wget", "-q", "--no-check-certificate", linkdatabase[1], "-O", name[1], NULL};
                    execv("/usr/bin/wget", argv);
                }
                else{
                    while((wait(&status_f4)) > 0);
                    char *argv[] = {"unzip", "-qq", name[1], NULL};
                    execv("/usr/bin/unzip", argv);
                }
            }
        }
        else{// gacha time
            while((wait(&status_f1)) > 0);
            k1 = 0, k2 = 0;
            DIR *d;
            struct dirent *dir;
            d = opendir("/home/januarevan/sisopmodul2/weapons");
            while ((dir = readdir(d)) != NULL) {
                if(strstr(dir->d_name, ".json") != NULL){
                    strcpy(weaponname[k1], dir->d_name);
                    k1++;
                }
            }
            closedir(d);
            d = opendir("/home/januarevan/sisopmodul2/characters");
            while ((dir = readdir(d)) != NULL) {
                if(strstr(dir->d_name, ".json") != NULL){
                    strcpy(charname[k2], dir->d_name);
                    k2++;
                }
            }
            closedir(d);
            pid_t f5 = fork();
            int status_f5;
            if(f5 < 0){
                exit(EXIT_FAILURE);
            }
            if(f5 == 0){
                char *argv[] = {"mkdir", "-p",  "gacha_gacha", NULL};
                execv("/usr/bin/mkdir", argv);
            }
            else{
                while((wait(&status_f5)) > 0);
                int primogems = 79000;
                int total;
                if(primogems % 160 == 0){
                    total = primogems / 160;
                }
                else{
                    total = primogems / 160;
                    total += 1;
                }
                if(total % 90 == 0){
                    total = total / 90;
                }
                else{
                    total = total / 90;
                    total += 1;
                }
                printf("ini init primos\n");
                pid_t f7 = fork();
                int status_f7;
                if(f7 < 0){
                    exit(EXIT_FAILURE);
                }
                if(f7 == 0){
                    forker(90*total, 1, total);
                    exit(0);
                }
                else{
                    while((wait(&status_f7)) > 0);
                    int total_gacha_folder = 0;
                    int total_gacha_txt = 0;
                    int total_gacha = 0;
                    char default_path[1024] = "/home/januarevan/sisopmodul2/gacha_gacha/";
                    char default_path_char[1024] = "/home/januarevan/sisopmodul2/characters/";
                    char default_path_weapon[1024] = "/home/januarevan/sisopmodul2/weapons/";
                    char default_gacha_folder[1024] = "total_gacha_";
                    char default_gacha_txt[1024] = "";
                    char default_gacha_list[1024] = "";
                    char temp_gacha_folder[1024] = "";
                    char temp_gacha_txt[1024] = "";
                    char temp_path[1024] = "";
                    while(primogems >= 160){
                        time_t t1 = time(NULL);
                        struct tm tm_now1 = *localtime(&t1);
                        printf("primogem berkurang\n");
                        if(total_gacha % 90 == 0){
                            strcpy(temp_path, default_path);
                            strcat(temp_path, default_gacha_folder);
                            total_gacha_folder += 90;
                            char temps[1024] = "";
                            sprintf(temps, "%d", total_gacha_folder);
                            strcat(temp_path, temps);
                            strcat(temp_path, "/");
                        }
                        printf("%s\n", temp_path);
                        if(total_gacha % 10 == 0){
                            total_gacha_txt += 10;
                            char temphour[1024] = "";
                            char tempmin[1024] = "";
                            char tempsec[1024] = "";
                            char total_gacha[1024] = "";
                            strcpy(temp_gacha_txt, temp_path);
                            sprintf(temphour, "%d", tm_now1.tm_hour);
                            sprintf(tempmin, "%d", tm_now1.tm_min);
                            sprintf(tempsec, "%d", tm_now1.tm_sec);
                            strcat(temp_gacha_txt, temphour);
                            strcat(temp_gacha_txt, ":");
                            strcat(temp_gacha_txt, tempmin);
                            strcat(temp_gacha_txt, ":");
                            strcat(temp_gacha_txt, tempsec);
                            strcat(temp_gacha_txt, "_gacha_");
                            sprintf(total_gacha, "%d", total_gacha_txt);
                            strcat(temp_gacha_txt, total_gacha);
                            strcat(temp_gacha_txt, ".txt");
                        }
                        FILE *fp1, *fp2;
                        int n;
                        char buffer[4096];
                        //open .json
                        int counter = 1;
                        while(counter <= 10 && primogems >= 160){
                            counter += 1;
                            total_gacha += 1;
                            primogems -= 160;
                            if(total_gacha % 2 == 1){
                                n = rand();
                                n %= 48;
                                char charsused[1024] = "";
                                strcpy(charsused, default_path_char);
                                strcat(charsused, charname[n]);
                                fp1 = fopen(charsused, "r");
                                fread(buffer, 4096, 1, fp1);
                                fclose(fp1);
                                fp2 = fopen(temp_gacha_txt, "a");
                                struct json_object *parsed_json;
                                struct json_object *name;
                                struct json_object *rarity;
                                parsed_json = json_tokener_parse(buffer);
                                json_object_object_get_ex(parsed_json, "name", &name);
                                json_object_object_get_ex(parsed_json, "rarity", &rarity);
                                fprintf(fp2,"%d_characters_%s_%s_%d\n", total_gacha, json_object_get_string(rarity), json_object_get_string(name), primogems);
                                fclose(fp2);
                            }
                            else{
                                n = rand();
                                n %= 130;
                                char weaponsused[1024] = "";
                                strcpy(weaponsused, default_path_weapon);
                                strcat(weaponsused, weaponname[n]);
                                fp1 = fopen(weaponsused, "r");
                                fread(buffer, 4096, 1, fp1);
                                fclose(fp1);
                                fp2 = fopen(temp_gacha_txt, "a");
                                struct json_object *parsed_json;
                                struct json_object *name;
                                struct json_object *rarity;
                                parsed_json = json_tokener_parse(buffer);
                                json_object_object_get_ex(parsed_json, "name", &name);
                                json_object_object_get_ex(parsed_json, "rarity", &rarity);
                                fprintf(fp2,"%d_weapons_%s_%s_%d\n", total_gacha, json_object_get_string(rarity), json_object_get_string(name), primogems);
                                fclose(fp2);
                            }
                        }
                        sleep(1);
                    }
                }
            }
        }
        exit(0);
    }
    else{ //sesudah gacha selesai
        while((wait(&status_gacha)) > 0);
        return;
    }
}

void gacha2(){
    pid_t gacha = fork();
    int status_gacha;
    
    if(gacha < 0){
        exit(EXIT_FAILURE);
    }
    if(gacha == 0){
        pid_t f1 = fork();
        int status_f1;
        if(f1 < 0){
            exit(EXIT_FAILURE);
        } 

        if(f1 == 0){//ngezip
            char *argv[]={"zip","-P","satuduatiga", "-r", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
            execv("/usr/bin/zip",argv);
        }
        else{//buang folder kosong
            while((wait(&status_f1)) > 0);
            pid_t f2 = fork();
            int status_f2;
            if(f2 < 0){
                exit(EXIT_FAILURE);
            } 
            if(f2 == 0){
                pid_t f3 = fork();
                int status_f3;
                if(f3 < 0){
                    exit(EXIT_FAILURE);
                } 
                if(f3 == 0){
                    pid_t f4 = fork();
                    int status_f4;
                    if(f4 < 0){
                        exit(EXIT_FAILURE);
                    }
                    if(f4 == 0){
                        pid_t f5 = fork();
                        int status_f5;
                        if(f5 < 0){
                            exit(EXIT_FAILURE);
                        }
                        if(f5 == 0){
                            char *argv[]={"rm","-r","characters.zip", NULL};
                            execv("/usr/bin/rm",argv);
                        }
                        else{
                            char *argv[]={"rm","-r","weapons.zip", NULL};
                            execv("/usr/bin/rm",argv);
                        }
                    }
                    else{
                        while((wait(&status_f4)) > 0);
                        char *argv[]={"rm","-r","characters", NULL};
                        execv("/usr/bin/rm",argv);
                    } 
                }
                else{
                    while((wait(&status_f3)) > 0);
                    char *argv[]={"rm","-r","weapons", NULL};
                    execv("/usr/bin/rm",argv);
                }
            }
            else{
                while((wait(&status_f2)) > 0);
                char *argv[]={"rm","-r","gacha_gacha", NULL};
                execv("/usr/bin/rm",argv);
            }
        }   
    }
    else{
        while((wait(&status_gacha)) > 0);
        return;
    }
}

int main (){
    pid_t pid, sid;
    pid = fork();
    if(pid < 0){
        exit(EXIT_FAILURE);
    }
    if(pid > 0){
        exit(EXIT_SUCCESS);
    }
    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/home/januarevan/sisopmodul2")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        time_t t = time(NULL);
        struct tm tm_now = *localtime(&t);
	    if(tm_now.tm_mon == 2 && tm_now.tm_mday == 30 && tm_now.tm_hour == 4 && tm_now.tm_min == 44 && !isDone){
            gacha1();
            isDone = true;
        }
        if(tm_now.tm_mon == 2 && tm_now.tm_mday == 30 && tm_now.tm_hour == 7 && tm_now.tm_min == 44 && isDone){
            gacha2();
            break;
        }
    }
    exit(0);
}
