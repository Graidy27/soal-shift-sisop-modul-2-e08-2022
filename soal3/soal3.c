#include <stdio.h>	//standar library in c language
#include <sys/types.h> 	//salah satunya untuk menggunakan getpid
#include <unistd.h>	//salah satunya untuk menggunakan getpid
#include <wait.h>	//untuk wait (3.b)
#include <stdlib.h>	//untuk exit
#include <dirent.h>	//untuk soal 3.c
#include <string.h>	//untuk function strstr, strcpy

char animal_name[50];
char darat_name[50][50];
char air_name[50][50];
char bird_name[50][50];
char path_modul2[] = "/home/graidy/modul2";
char path_animal[] = "/home/graidy/modul2/animal/";
char path_darat[] = "/home/graidy/modul2/darat/";
char path_air[] = "/home/graidy/modul2/air/";
int darat = 0, air = 0, bird = 0;

//nomor 3.a
void mkdir_darat(){
	char *argv[] = {"mkdir", "-p", "darat", NULL};
	execv("/usr/bin/mkdir", argv);
}

void mkdir_air(){
	char *argv[] = {"mkdir", "-p", "air", NULL};
	execv("/usr/bin/mkdir", argv);
}

void tiga_a(){
	pid_t child_id;
	int status;
	child_id = fork();

	if (child_id < 0) exit(EXIT_FAILURE);
	if (child_id == 0) mkdir_darat();		//Membuat folder darat
	else {
		while((wait(&status)) > 0);
		sleep(3);				//Menunggu 3 detik
		mkdir_air();				//Membuat folder air
	}
}

//nomor 3.b
void tiga_b(){
	//printf("\nMasuk soal 3b\n");
	char *argv[] = {"unzip", "animal.zip", NULL};
	execv("/usr/bin/unzip", argv);			//Meng-unzip file animal.zip
}

//nomor 3.c
void grouping_animal(){
	DIR *dp;
	struct dirent *ls;
	dp = opendir(path_animal);
	int i = 0;
	while ((ls = readdir(dp)) != NULL) {
		strcpy(animal_name, ls->d_name);
		if(strstr(animal_name, "darat")){
			strcpy(darat_name[darat], animal_name);
			//printf("Darat: %s\n", darat_name[darat]);
			darat++;
            	}
		if(strstr(animal_name, "air")){
			strcpy(air_name[air], animal_name);
			//printf("Air: %s\n", air_name[air]);
			air++;
		}
		if(strstr(animal_name, "bird")){
			strcpy(bird_name[bird], animal_name);
			//printf("Bird: %s\n", bird_name[bird]);
			bird++;
		}
	}
	closedir(dp);
}

void mv_darat(int total){
	//printf("%s\n", darat_name[darat]);
	pid_t mv_darat_id;
	int mv_darat_status;
	mv_darat_id = fork();
	if(mv_darat_id < 0) exit(EXIT_FAILURE);
	else if(mv_darat_id == 0){
		if(darat-total > 0)	mv_darat(total+1);
		char path_pict[100];
		strcpy(path_pict, path_animal);
		strcat(path_pict, darat_name[darat-total]);
		//printf("%s\n", path_pict);
		char *argv[] = {"mv", path_pict, path_darat, NULL};
		execv("/usr/bin/mv", argv);
	}
	else while((wait(&mv_darat_status)) > 0);
}

void mv_air(int total){
        //printf("%s\n", air_name[darat]);
        pid_t mv_air_id;
        int mv_air_status;
        mv_air_id = fork();
        if(mv_air_id < 0) exit(EXIT_FAILURE);
        else if(mv_air_id == 0){
                if(air-total > 0)   mv_air(total+1);
                char path_pict[100];
                strcpy(path_pict, path_animal);
                strcat(path_pict, air_name[air-total]);
                //printf("%s\n", path_pict);
                char *argv[] = {"mv", path_pict, path_air, NULL};
                execv("/usr/bin/mv", argv);
        }
        else while((wait(&mv_air_status)) > 0);
}

void tiga_c(){
	grouping_animal();
	mv_darat(1);
	mv_air(1);
}


//nomor 3.d
void rm_bird(int total){
        pid_t rm_bird_id;
        int rm_bird_status;
        rm_bird_id = fork();
        if(rm_bird_id < 0) exit(EXIT_FAILURE);
        else if(rm_bird_id == 0){
                if(bird-total > 0)   rm_bird(total+1);
                char path_pict[100];
                strcpy(path_pict, path_darat);
                strcat(path_pict, bird_name[bird-total]);
                //printf("%d %s\n", bird, path_pict);
                char *argv[] = {"rm", path_pict, NULL};
                execv("/usr/bin/rm", argv);
        }
        else while((wait(&rm_bird_status)) > 0);
}

void tiga_d(){
	rm_bird(1);
}

//nomor 3.e
void tiga_e(){
	FILE* fp;
	fp = fopen("/home/graidy/modul2/air/list.txt", "a");
	int result;
	uid_t uid = getuid();
	for(int j = 0; j < air; j++){
		fprintf(fp, "%d_", uid);
		result = access ("/home/graidy/modul2/air/list.txt", R_OK);
		if(result == 0){
			fprintf(fp, "r");
		}
		result = access ("/home/graidy/modul2/air/list.txt", W_OK);
		if(result == 0){
			fprintf(fp, "w");
		}
		result = access ("/home/graidy/modul2/air/list.txt", X_OK);
		if(result == 0){
			fprintf(fp, "x");
		}
		fprintf(fp, "_%s\n", air_name[j]);
	}
}


int main() {
	pid_t main_child_id;
	int main_status;
	main_child_id = fork();

        if (main_child_id < 0) exit(EXIT_FAILURE);

	//Run 3.a
        if (main_child_id == 0) tiga_a();
        else {
		while((wait(&main_status)) > 0);

		pid_t main2_child_id;
		int main2_status;
		main2_child_id = fork();

		if (main2_child_id < 0) exit(EXIT_FAILURE);

		//Run 3.b
		if (main2_child_id == 0) tiga_b();

		else{
			while((wait(&main2_status)) > 0);
			pid_t main3_child_id;
			int main3_status;
			main3_child_id = fork();

			if (main3_child_id < 0) exit(EXIT_FAILURE);

			//Run 3.c, 3.d, 3.e
			if(main3_child_id == 0) {tiga_c(); tiga_d(); tiga_e();}

			else{
				while((wait(&main3_status)) > 0);
			}
		}
        }
}
