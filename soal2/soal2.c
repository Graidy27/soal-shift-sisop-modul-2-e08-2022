#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>

//fungsi fungsi
void unzip (char *zipfile, char *dir);
void mkdir (char *dir);
void rm (char *file, char *dir);
void modifyfilename (char *dir);
void makefolder (char *dir, char kategori[10]);
void delpng (char *str, char *substr);
void movetofolder (char *namafile, char path[], char *file, char *kategori);
void list(char kategori[10], char nama[50], char tahun[5], char path[]);
void hapus (char *name, char path[]);

char defaultpath[FILENAME_MAX] = "/home/helmitaqiyudin/shift2/drakor/";
char listofcategories[16][FILENAME_MAX];

struct listfilm{
  char name[FILENAME_MAX];
  int year;
  char category[FILENAME_MAX];
};

struct listfilm films[128];

int comp(const void *p, const void *q) {
  struct listfilm *p1 = (struct listfilm*)p;
  struct listfilm *p2 = (struct listfilm*)q;

  int val_1 = p1->year;
  int val_2 = p2->year;

  if (p1->year < p2->year) return 1;
  if (p1->year > p2->year) return -1;
  return 0;
}

int main() 
{ 
  char* fileloc = "/home/helmitaqiyudin/drakor.zip";
  char* dir = "/home/helmitaqiyudin/shift2/drakor";

  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) 
  {
    exit(EXIT_FAILURE); 
  }

  if (child_id == 0) 
  {
    // this is child
    mkdir(dir); // #1 membuat direktori untuk menyimpan file
  } 
  else 
  {
    // this is parent
    while ((wait(&status)) > 0);
    pid_t child_id;
    int status;
    
    child_id = fork();

    if (child_id < 0) 
    {
      exit(EXIT_FAILURE); 
    }
  
    if (child_id == 0) 
    {
      // this is child
      unzip(fileloc, dir); //#2. Unzip file
    } 
    else {
      // this is parent
      while ((wait(&status)) > 0);
      pid_t child_id;
      int status;

      child_id = fork();

      if (child_id < 0) 
      {
        exit(EXIT_FAILURE); 
      }

      if (child_id == 0) {
        // this is child
        rm(fileloc, dir); //#3. Hapus file & folder yang tidak diperlukan 
      } 
      else 
      { 
        // this is parent
        while ((wait(&status)) > 0);
        modifyfilename(dir); 
      }
    }
  }
}

void unzip (char *zipfile, char *dir)
{
  char *argv[] = { "unzip", "-o", zipfile, "-d", dir, NULL };
  execv ("/usr/bin/unzip", argv);
}

void mkdir (char *dir)
{
  char *argv[] = { "mkdir", "-p", dir, NULL };
  execv ("/bin/mkdir", argv);
}

void rm (char *file, char *dir)
{
  char *argv[] = { "find" , dir, "-mindepth", "1", "-type", "d", "-exec", "rm", "-rf", "{}", ";", NULL };
  execv ("/bin/find", argv);
}

void modifyfilename (char *dir)
{
  DIR *dp;
 struct dirent *ep;
 char path[1000] = "";
  strcpy(path, dir);

  dp = opendir(path);
  int f1 = 0;
  if (dp != NULL)
 {
    while ((ep = readdir (dp)))
    {
      if(strcmp(ep->d_name, ".") !=0 && strcmp(ep->d_name, "..") !=0) //!
      {
        char filename[100];
        strcpy(filename, ep->d_name);
        delpng(filename, ".png");

        char nama[50]; char tahun[5]; char kategori [10];
        int tahun_int;
        
        char *split;
        split = strtok(filename, "_;");

        int mark = 0;
        while (split != NULL)
        {
          if (mark == 0) 
          {
            strcpy(nama, split);
          }
          else if (mark == 1 )
          {
            strcpy(tahun, split);
            tahun_int = atoi(tahun);
          }
          else if (mark == 2 )
          { 
            strcpy(kategori, split);
            makefolder(path, kategori);
            movetofolder(ep->d_name, path, nama, kategori);
            strcpy(films[f1].category, kategori);
            films[f1].year = tahun_int;
            strcpy(films[f1].name, nama);
            f1 += 1;
          }
          split = strtok(NULL, "_;");
          mark++;
        }
        hapus(ep->d_name,path);
      }
    }
  }
  size_t a = sizeof(struct listfilm);
  qsort(films, f1, a, comp);
  int num_of_categories = 0;
  strcpy(path, dir);
  dp = opendir(path);
  if(dp != NULL){
    while ((ep = readdir (dp)))
    {
      if(strcmp(ep->d_name, ".") !=0 && strcmp(ep->d_name, "..") !=0){ //!
        char filename[100];
        strcpy(filename, ep->d_name);
        strcpy(listofcategories[num_of_categories], filename);
        num_of_categories += 1;
      }
    }
  }
  closedir(dp);
  pid_t f5;
  int status_f5;
  FILE *da;
  for (int i = 0; i < f1; i++){
    char temp[FILENAME_MAX];
    strcpy(temp, defaultpath);
    strcat(temp, "/");
    strcat(temp, films[i].category);
    strcat(temp, "/");
    strcat(temp, "data.txt");
    da = fopen(temp, "a");
    fprintf(da, "nama : %s\nrilis : tahun %d\n\n", films[i].name, films[i].year);
  }
  fclose(da);
  for(int i = 0; i < num_of_categories; i++){
    char temp[FILENAME_MAX];
    strcpy(temp, defaultpath);
    strcat(temp, "/");
    strcat(temp, listofcategories[i]);
    strcat(temp, "/");
    strcat(temp, "data.txt");
    da = fopen(temp, "a");
    fprintf(da, "kategori : %s\n\n", listofcategories[i]);
  }
  fclose(da);
}

void makefolder (char path[], char kategori[10])
{
  char folder[300]="";
  strcpy(folder, path);
  strcat(folder,"/");
  strcat(folder, kategori);

  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"mkdir", "-p", folder, NULL};
    execv("/bin/mkdir", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
    FILE *fp;
  char pathdata[1000]="";

  strcpy(pathdata, path);
  strcat(pathdata, "/");
  strcat(pathdata, kategori);
  strcat(pathdata, "/data.txt");
  return;
  }
}

void delpng(char *str, char *substr)
{
 char *sama;
 int pjg = strlen(substr);
 while ((sama = strstr(str, substr))) 
    {
       *sama = '\0';
       strcat(str, sama+pjg);
 }
}

void movetofolder (char *namafile, char path[], char *file, char *kategori)
{
  char before[100]="";
  strcpy(before, path);
  strcat(before, "/");
  strcat(before, namafile);
 
  char after[100]="";
  strcat(after, path);
  strcat(after, "/");
  strcat(after, kategori);
  strcat(after, "/");
  strcat(after, file);
  strcat(after, ".png");

  pid_t child_id = fork();
 int status;

 if (child_id < 0) 
  {
   exit(EXIT_FAILURE);
 }

 if (child_id == 0) 
  {
    char *argv[] = {"cp", before, after, NULL};
    execv("/bin/cp", argv);
 }
  else 
  {
    while((wait(&status)) > 0);
    return;
 }
}

void list(char kategori[10], char nama[50], char tahun[5], char path[])
{
  FILE *fp;
  char pathdata[1000]="";

  strcpy(pathdata, path);
  strcat(pathdata, "/");
  strcat(pathdata, kategori);
  strcat(pathdata, "/data.txt");

  fp = fopen(pathdata, "a");
  fprintf(fp, "nama : %s\ntahun : %s\n\n", nama, tahun);
  fclose(fp);
  return;
}

void hapus (char *name, char path[])
{
  char before[100]="";
  strcpy(before, path);
  strcat(before, "/");
  strcat(before, name);

  pid_t child_id = fork();
  int status;

  if (child_id < 0) 
  {
   exit(EXIT_FAILURE);
  }

  if (child_id == 0) 
  {
    char *argv[] = {"rm", before, NULL};
    execv("/bin/rm", argv);
  }
  else 
  {
    while((wait(&status)) > 0);
    return;
  }
}
