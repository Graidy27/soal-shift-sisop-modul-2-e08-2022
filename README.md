# Soal Shift Modul 2 Kelompok E08

Anggota :
> Januar Evan - 5025201210\
> Graidy Megananda - 5025201188\
> Helmi Taqiyudin - 5025201152

---
## Tabel Konten
- [Soal 1](#nomor-1)
- [Soal 2](#nomor-2)
- [Soal 3](#nomor-3)
  - [Soal 3.a](#3a)
  - [Soal 3.b](#3b)
  - [Soal 3.c](#3c)
  - [Soal 3.d](#3d)
  - [Soal 3.e](#3e)


## Nomor 1
> Step By Step
1. fungsi gacha1
    fungsi ini membuat beberapa child process karena penggunaan execv yang mengharuskan untuk menggunakan child process
    Child process yang pertama kali dilakukan adalah download file characters.zip
    ```c
    if(f3 == 0){
        char *argv[] = {"wget", "-q", "--no-check-certificate", linkdatabase[0], "-O", name[0], NULL};
        execv("/usr/bin/wget", argv);
    }
    else{
        while((wait(&status_f3)) > 0);
        char *argv[] = {"unzip", "-qq", name[0], NULL};
        execv("/usr/bin/unzip", argv);
    }
    ```
    kemudian download file weapons.zip
    ```c
    if(f4 == 0){
        char *argv[] = {"wget", "-q", "--no-check-certificate", linkdatabase[1], "-O", name[1], NULL};
        execv("/usr/bin/wget", argv);
    }
    else{
        while((wait(&status_f4)) > 0);
        char *argv[] = {"unzip", "-qq", name[1], NULL};
        execv("/usr/bin/unzip", argv);
    }
    ```
    Setelah itu membaca nama - nama file yang ada di dalam weapons.zip dan characters.zip
    ```c
    while((wait(&status_f1)) > 0);
    k1 = 0, k2 = 0;
    DIR *d;
    struct dirent *dir;
    d = opendir("/home/januarevan/sisopmodul2/weapons");
    while ((dir = readdir(d)) != NULL) {
        if(strstr(dir->d_name, ".json") != NULL){
            strcpy(weaponname[k1], dir->d_name);
            k1++;
        }
    }
    closedir(d);
    d = opendir("/home/januarevan/sisopmodul2/characters");
    while ((dir = readdir(d)) != NULL) {
        if(strstr(dir->d_name, ".json") != NULL){
            strcpy(charname[k2], dir->d_name);
            k2++;
        }
    }
    closedir(d);
    ```
    Kemudian membuat child process lagi untuk menggunakan execv mkdir
    ```c
    if(f5 == 0){
        char *argv[] = {"mkdir", "-p",  "gacha_gacha", NULL};
        execv("/usr/bin/mkdir", argv);
    }
    ```
    Setelah itu melakukan gacha primogem yang telah ditentukan
    Hal pertama yang dilakukan adalah menghitung berapa banyak folder yang harus karena nanti akan melakukan pemanggilan fungsi rekursif untuk membuat child process sebanyak n buah (pemanggilan fungsi sebanyak n kali)
    ```c
    int primogems = 79000;
    int total;
    if(primogems % 160 == 0){
        total = primogems / 160;
    }
    else{
        total = primogems / 160;
        total += 1;
    }
    if(total % 90 == 0){
        total = total / 90;
    }
    else{
        total = total / 90;
        total += 1;
    }
    ```
    Setelah itu membuat child process lagi untuk memanggil fungsi rekursif forker()
    ```c
    void forker(int total, int temp, int nprocesses){
    pid_t pid = fork();
    int status_pid;
    if(pid < 0){
        exit(EXIT_FAILURE);
    }
    if(pid == 0){
        if(nprocesses == temp){
        }
        else{
            forker(total - 90, temp+1, nprocesses);
        }
        char temp_path[1024];
        strcat(temp_path, "/home/januarevan/sisopmodul2/gacha_gacha/");
        strcat(temp_path, "total_gacha_");
        char temps[1024] = "";
        sprintf(temps, "%d", total);
        strcat(temp_path, temps);
        char *argv[] = {"mkdir", "-p", temp_path, NULL};
        execv("/usr/bin/mkdir", argv);
    }
    else{
        while((wait(&status_pid)) > 0);
        return;
    }
    ```
    ```c
    if(f7 == 0){
        forker(90*total, 1, total);
        exit(0);
    }
    ```
    Kemudian melakukan pembuatan .txt dengan isi 10 item tiap .txt dan 9 .txt setiap folder gacha
    ```c
    else{
        while((wait(&status_f5)) > 0);
        int primogems = 79000;
        int total;
        if(primogems % 160 == 0){
            total = primogems / 160;
        }
        else{
            total = primogems / 160;
            total += 1;
        }
        if(total % 90 == 0){
            total = total / 90;
        }
        else{
            total = total / 90;
            total += 1;
        }
        printf("ini init primos\n");
        pid_t f7 = fork();
        int status_f7;
        if(f7 < 0){
            exit(EXIT_FAILURE);
        }
        if(f7 == 0){
            forker(90*total, 1, total);
            exit(0);
        }
        else{
            while((wait(&status_f7)) > 0);
            int total_gacha_folder = 0;
            int total_gacha_txt = 0;
            int total_gacha = 0;
            char default_path[1024] = "/home/januarevan/sisopmodul2/gacha_gacha/";
            char default_path_char[1024] = "/home/januarevan/sisopmodul2/characters/";
            char default_path_weapon[1024] = "/home/januarevan/sisopmodul2/weapons/";
            char default_gacha_folder[1024] = "total_gacha_";
            char default_gacha_txt[1024] = "";
            char default_gacha_list[1024] = "";
            char temp_gacha_folder[1024] = "";
            char temp_gacha_txt[1024] = "";
            char temp_path[1024] = "";
            while(primogems >= 160){
                time_t t1 = time(NULL);
                struct tm tm_now1 = *localtime(&t1);
                printf("primogem berkurang\n");
                if(total_gacha % 90 == 0){
                    strcpy(temp_path, default_path);
                    strcat(temp_path, default_gacha_folder);
                    total_gacha_folder += 90;
                    char temps[1024] = "";
                    sprintf(temps, "%d", total_gacha_folder);
                    strcat(temp_path, temps);
                    strcat(temp_path, "/");
                }
                printf("%s\n", temp_path);
                if(total_gacha % 10 == 0){
                    total_gacha_txt += 10;
                    char temphour[1024] = "";
                    char tempmin[1024] = "";
                    char tempsec[1024] = "";
                    char total_gacha[1024] = "";
                    strcpy(temp_gacha_txt, temp_path);
                    sprintf(temphour, "%d", tm_now1.tm_hour);
                    sprintf(tempmin, "%d", tm_now1.tm_min);
                    sprintf(tempsec, "%d", tm_now1.tm_sec);
                    strcat(temp_gacha_txt, temphour);
                    strcat(temp_gacha_txt, ":");
                    strcat(temp_gacha_txt, tempmin);
                    strcat(temp_gacha_txt, ":");
                    strcat(temp_gacha_txt, tempsec);
                    strcat(temp_gacha_txt, "_gacha_");
                    sprintf(total_gacha, "%d", total_gacha_txt);
                    strcat(temp_gacha_txt, total_gacha);
                    strcat(temp_gacha_txt, ".txt");
                }
                FILE *fp1, *fp2;
                int n;
                char buffer[4096];
                //open .json
                int counter = 1;
                while(counter <= 10 && primogems >= 160){
                    counter += 1;
                    total_gacha += 1;
                    primogems -= 160;
                    if(total_gacha % 2 == 1){
                        n = rand();
                        n %= 48;
                        char charsused[1024] = "";
                        strcpy(charsused, default_path_char);
                        strcat(charsused, charname[n]);
                        fp1 = fopen(charsused, "r");
                        fread(buffer, 4096, 1, fp1);
                        fclose(fp1);
                        fp2 = fopen(temp_gacha_txt, "a");
                        struct json_object *parsed_json;
                        struct json_object *name;
                        struct json_object *rarity;
                        parsed_json = json_tokener_parse(buffer);
                        json_object_object_get_ex(parsed_json, "name", &name);
                        json_object_object_get_ex(parsed_json, "rarity", &rarity);
                        fprintf(fp2,"%d_characters_%s_%s_%d\n", total_gacha, json_object_get_string(rarity), json_object_get_string(name), primogems);
                        fclose(fp2);
                    }
                    else{
                        n = rand();
                        n %= 130;
                        char weaponsused[1024] = "";
                        strcpy(weaponsused, default_path_weapon);
                        strcat(weaponsused, weaponname[n]);
                        fp1 = fopen(weaponsused, "r");
                        fread(buffer, 4096, 1, fp1);
                        fclose(fp1);
                        fp2 = fopen(temp_gacha_txt, "a");
                        struct json_object *parsed_json;
                        struct json_object *name;
                        struct json_object *rarity;
                        parsed_json = json_tokener_parse(buffer);
                        json_object_object_get_ex(parsed_json, "name", &name);
                        json_object_object_get_ex(parsed_json, "rarity", &rarity);
                        fprintf(fp2,"%d_weapons_%s_%s_%d\n", total_gacha, json_object_get_string(rarity), json_object_get_string(name), primogems);
                        fclose(fp2);
                    }
                }
                sleep(1);
            }
        }
    ```
    2. gacha2()
    Penggunaan fungsi gacha2() adalah menghapus weapons.zip, characters.zip, folder characters, folder weapons, zip folder gacha-gacha, dan menghapus folder gacha-gacha.
    ```c
    if(gacha < 0){
        exit(EXIT_FAILURE);
    }
    if(gacha == 0){
        pid_t f1 = fork();
        int status_f1;
        if(f1 < 0){
            exit(EXIT_FAILURE);
        } 
        if(f1 == 0){//ngezip
            char *argv[]={"zip","-P","satuduatiga", "-r", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
            execv("/usr/bin/zip",argv);
        }
        else{//buang folder kosong
            while((wait(&status_f1)) > 0);
            pid_t f2 = fork();
            int status_f2;
            if(f2 < 0){
                exit(EXIT_FAILURE);
            } 
            if(f2 == 0){
                pid_t f3 = fork();
                int status_f3;
                if(f3 < 0){
                    exit(EXIT_FAILURE);
                } 
                if(f3 == 0){
                    pid_t f4 = fork();
                    int status_f4;
                    if(f4 < 0){
                        exit(EXIT_FAILURE);
                    }
                    if(f4 == 0){
                        pid_t f5 = fork();
                        int status_f5;
                        if(f5 < 0){
                            exit(EXIT_FAILURE);
                        }
                        if(f5 == 0){
                            char *argv[]={"rm","-r","characters.zip", NULL};
                            execv("/usr/bin/rm",argv);
                        }
                        else{
                            char *argv[]={"rm","-r","weapons.zip", NULL};
                            execv("/usr/bin/rm",argv);
                        }
                    }
                    else{
                        while((wait(&status_f4)) > 0);
                        char *argv[]={"rm","-r","characters", NULL};
                        execv("/usr/bin/rm",argv);
                    } 
                }
                else{
                    while((wait(&status_f3)) > 0);
                    char *argv[]={"rm","-r","weapons", NULL};
                    execv("/usr/bin/rm",argv);
                }
            }
            else{
                while((wait(&status_f2)) > 0);
                char *argv[]={"rm","-r","gacha_gacha", NULL};
                execv("/usr/bin/rm",argv);
            }
        }   
    }
    else{
        while((wait(&status_gacha)) > 0);
        return;
    }
    ```
    3. Main
    dari main, digunakan library time.h untuk mendapatkan waktu yang dibutuhkan untuk melakukan gacha pada tanggal 30 Maret jam 04:44 kemudian menghapus semua folder menyisakan file zip not_safe_for_wibu dengan password "satuduatiga"
    ```c
    if(tm_now.tm_mon == 2 && tm_now.tm_mday == 30 && tm_now.tm_hour == 4 && tm_now.tm_min == 44 && !isDone){
        gacha1();
        isDone = true;
    }
    if(tm_now.tm_mon == 2 && tm_now.tm_mday == 30 && tm_now.tm_hour == 7 && tm_now.tm_min == 44 && isDone){
        gacha2();
        break;
    }
    ```
> Masalah yang dihadapi
1. Penggunaan json parser
pada bagian source code yang digunakan dari youtube adalah char buffer[1024] kurang besar, jadi harus menghasilkan null
![](pic/image__12_.png)
2. Penggunaan json parser
Segmentation fault
![](pic/image__11_.png)
## Nomor 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

Pertama, kita lakukan inisiasi untuk fungsi - fungsi yang digunakan, 
```c
//fungsi fungsi
void unzip (char *zipfile, char *dir);
void mkdir (char *dir);
void rm (char *file, char *dir);
void modifyfilename (char *dir);
void makefolder (char *dir, char kategori[10]);
void delpng (char *str, char *substr);
void movetofolder (char *namafile, char path[], char *file, char *kategori);
void list(char kategori[10], char nama[50], char tahun[5], char path[]);
void hapus (char *name, char path[]);

char defaultpath[FILENAME_MAX] = "/home/helmitaqiyudin/shift2/drakor/";
char listofcategories[16][FILENAME_MAX];

struct listfilm{
  char name[FILENAME_MAX];
  int year;
  char category[FILENAME_MAX];
};

struct listfilm films[128];

int comp(const void *p, const void *q) {
  struct listfilm *p1 = (struct listfilm*)p;
  struct listfilm *p2 = (struct listfilm*)q;

  int val_1 = p1->year;
  int val_2 = p2->year;

  if (p1->year < p2->year) return 1;
  if (p1->year > p2->year) return -1;
  return 0;
}
```
fungsi main / fungsi awalan 
```c
int main() 
{ 
  char* fileloc = "/home/helmitaqiyudin/drakor.zip";
  char* dir = "/home/helmitaqiyudin/shift2/drakor";

  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) 
  {
    exit(EXIT_FAILURE); 
  }

  if (child_id == 0) 
  {
    // this is child
    mkdir(dir); // #1 membuat direktori untuk menyimpan file
  } 
  else 
  {
    // this is parent
    while ((wait(&status)) > 0);
    pid_t child_id;
    int status;
    
    child_id = fork();

    if (child_id < 0) 
    {
      exit(EXIT_FAILURE); 
    }
  
    if (child_id == 0) 
    {
      // this is child
      unzip(fileloc, dir); //#2. Unzip file
    } 
    else {
      // this is parent
      while ((wait(&status)) > 0);
      pid_t child_id;
      int status;

      child_id = fork();

      if (child_id < 0) 
      {
        exit(EXIT_FAILURE); 
      }

      if (child_id == 0) {
        // this is child
        rm(fileloc, dir); //#3. Hapus file & folder yang tidak diperlukan 
      } 
      else 
      { 
        // this is parent
        while ((wait(&status)) > 0);
        modifyfilename(dir); 
      }
    }
  }
}

```
dalam fungsi main ini langkah langkah yang kita lakukan adalah membuat direktori untuk menampung file dari drakor.zip, kemudian mengunzip drakor.zip ke folder yang sudah dibuat. lalu memfilter file file sampah dan file file yang penting.

Selanjutnya kita definisikan fungsi fungsi yang digunakan

```c

void unzip (char *zipfile, char *dir)
{
  char *argv[] = { "unzip", "-o", zipfile, "-d", dir, NULL };
  execv ("/usr/bin/unzip", argv);
}

void mkdir (char *dir)
{
  char *argv[] = { "mkdir", "-p", dir, NULL };
  execv ("/bin/mkdir", argv);
}

void rm (char *file, char *dir)
{
  char *argv[] = { "find" , dir, "-mindepth", "1", "-type", "d", "-exec", "rm", "-rf", "{}", ";", NULL };
  execv ("/bin/find", argv);
}

void modifyfilename (char *dir)
{
  DIR *dp;
 struct dirent *ep;
 char path[1000] = "";
  strcpy(path, dir);

  dp = opendir(path);
  int f1 = 0;
  if (dp != NULL)
 {
    while ((ep = readdir (dp)))
    {
      if(strcmp(ep->d_name, ".") !=0 && strcmp(ep->d_name, "..") !=0) //!
      {
        char filename[100];
        strcpy(filename, ep->d_name);
        delpng(filename, ".png");

        char nama[50]; char tahun[5]; char kategori [10];
        int tahun_int;
        
        char *split;
        split = strtok(filename, "_;");

        int mark = 0;
        while (split != NULL)
        {
          if (mark == 0) 
          {
            strcpy(nama, split);
          }
          else if (mark == 1 )
          {
            strcpy(tahun, split);
            tahun_int = atoi(tahun);
          }
          else if (mark == 2 )
          { 
            strcpy(kategori, split);
            makefolder(path, kategori);
            movetofolder(ep->d_name, path, nama, kategori);
            strcpy(films[f1].category, kategori);
            films[f1].year = tahun_int;
            strcpy(films[f1].name, nama);
            f1 += 1;
          }
          split = strtok(NULL, "_;");
          mark++;
        }
        hapus(ep->d_name,path);
      }
    }
  }
  size_t a = sizeof(struct listfilm);
  qsort(films, f1, a, comp);
  int num_of_categories = 0;
  strcpy(path, dir);
  dp = opendir(path);
  if(dp != NULL){
    while ((ep = readdir (dp)))
    {
      if(strcmp(ep->d_name, ".") !=0 && strcmp(ep->d_name, "..") !=0){ //!
        char filename[100];
        strcpy(filename, ep->d_name);
        strcpy(listofcategories[num_of_categories], filename);
        num_of_categories += 1;
      }
    }
  }
  closedir(dp);
  pid_t f5;
  int status_f5;
  FILE *da;
  for (int i = 0; i < f1; i++){
    char temp[FILENAME_MAX];
    strcpy(temp, defaultpath);
    strcat(temp, "/");
    strcat(temp, films[i].category);
    strcat(temp, "/");
    strcat(temp, "data.txt");
    da = fopen(temp, "a");
    fprintf(da, "nama : %s\nrilis : tahun %d\n\n", films[i].name, films[i].year);
  }
  fclose(da);
  for(int i = 0; i < num_of_categories; i++){
    char temp[FILENAME_MAX];
    strcpy(temp, defaultpath);
    strcat(temp, "/");
    strcat(temp, listofcategories[i]);
    strcat(temp, "/");
    strcat(temp, "data.txt");
    da = fopen(temp, "a");
    fprintf(da, "kategori : %s\n\n", listofcategories[i]);
  }
  fclose(da);
}

void makefolder (char path[], char kategori[10])
{
  char folder[300]="";
  strcpy(folder, path);
  strcat(folder,"/");
  strcat(folder, kategori);

  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"mkdir", "-p", folder, NULL};
    execv("/bin/mkdir", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
    FILE *fp;
  char pathdata[1000]="";

  strcpy(pathdata, path);
  strcat(pathdata, "/");
  strcat(pathdata, kategori);
  strcat(pathdata, "/data.txt");
  return;
  }
}

void delpng(char *str, char *substr)
{
 char *sama;
 int pjg = strlen(substr);
 while ((sama = strstr(str, substr))) 
    {
       *sama = '\0';
       strcat(str, sama+pjg);
 }
}

void movetofolder (char *namafile, char path[], char *file, char *kategori)
{
  char before[100]="";
  strcpy(before, path);
  strcat(before, "/");
  strcat(before, namafile);
 
  char after[100]="";
  strcat(after, path);
  strcat(after, "/");
  strcat(after, kategori);
  strcat(after, "/");
  strcat(after, file);
  strcat(after, ".png");

  pid_t child_id = fork();
 int status;

 if (child_id < 0) 
  {
   exit(EXIT_FAILURE);
 }

 if (child_id == 0) 
  {
    char *argv[] = {"cp", before, after, NULL};
    execv("/bin/cp", argv);
 }
  else 
  {
    while((wait(&status)) > 0);
    return;
 }
}

void list(char kategori[10], char nama[50], char tahun[5], char path[])
{
  FILE *fp;
  char pathdata[1000]="";

  strcpy(pathdata, path);
  strcat(pathdata, "/");
  strcat(pathdata, kategori);
  strcat(pathdata, "/data.txt");

  fp = fopen(pathdata, "a");
  fprintf(fp, "nama : %s\ntahun : %s\n\n", nama, tahun);
  fclose(fp);
  return;
}

void hapus (char *name, char path[])
{
  char before[100]="";
  strcpy(before, path);
  strcat(before, "/");
  strcat(before, name);

  pid_t child_id = fork();
  int status;

  if (child_id < 0) 
  {
   exit(EXIT_FAILURE);
  }

  if (child_id == 0) 
  {
    char *argv[] = {"rm", before, NULL};
    execv("/bin/rm", argv);
  }
  else 
  {
    while((wait(&status)) > 0);
    return;
  }
}

```
Fungsi `unzip` kita gunakan untuk mengeluarkan file dari file zip. fungsi `mkdir` digunakan untuk membuat folder untuk menampung file - file yang di ekstrak dari file zip. `rm` kita gunakan untuk memfilter antara file sampah dengan file yang akan kita gunakan. `modifyfilename` adalah fungsi lanjutan yang berisi fungsi - fungsi lainnya yaitu, `delpng`digunakan untuk menghapus format png untuk memudahkan kita mengeksekusi file. `makefolder` membuat folder masing masing genre, kemudian `movetofolder` memasukkan file ke folder masing - masing genre dengan cara di-copy, `hapus` digunakan untuk menghapus file diluar folder genre, merename file sesuai dengan ketentuan, dan membuat file "data.txt" yang berisi data - data file tiap folder yang diurutkan berdasarkan tahunnya.

###Hasil
![image](/uploads/511a8f588c1521ada12e47640ff6b276/image.png)
![image](/uploads/9de8ecdd77afbc2f2489f84daad15643/image.png)
![image](/uploads/3fd0449d75b3082e99de999f55251521/image.png)
![image](/uploads/fb55dfca7ec82210392a54a6c55b6f30/image.png)

## Nomor 3

**[Source Code Soal 3](https://gitlab.com/Graidy27/soal-shift-sisop-modul-2-e08-2022/-/blob/main/soal3/soal3.c)**

**Persiapan**
- Variabel `animal_name` digunakan untuk menyimpan nama foto sementara
- Variabel `darat_name` digunakan untuk menyimpan semua judul foto yang mengandung kata `darat`
- Variabel `air_name` digunakan untuk menyimpan semua judul foto yang mengandung kata `air`
- Variabel `bird_name` digunakan untuk menyimpan semua foto yang mengandung kata `bird`
- Variabel `path_modul2` digunakan untuk menyimpan path ke folder bernama `modul2`
- Variabel `path_animal` digunakan untuk menyimpan path ke folder bernama `animal` (folder yang awalnya berisi semua foto)
- Variabel `path_darat` digunakan untuk menyimpan path ke folder bernama `darat`
- Variabel `path_air` digunakan untuk menyimpan path ke folder bernama `air`
- Variabel `darat`, `air`, dan `bird` berturut-turut digunakan untuk menghitung banyaknya foto yang mengandung kata `darat`, `air`, dan `bird`.
- `int main()` hanya digunakan untuk menjalankan function yang masing-masing akan menjawab soal a, b, c, d, dan e dengan memanfaatkan [fork](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul2#fork).
```
int main() {
	pid_t main_child_id;
	int main_status;
	main_child_id = fork();

    if (main_child_id < 0) exit(EXIT_FAILURE);

	//Run 3.a
    if (main_child_id == 0) tiga_a();
    else {
	    while((wait(&main_status)) > 0);

		pid_t main2_child_id;
		int main2_status;
		main2_child_id = fork();

		if (main2_child_id < 0) exit(EXIT_FAILURE);

		//Run 3.b
		if (main2_child_id == 0) tiga_b();

		else{
			while((wait(&main2_status)) > 0);
			pid_t main3_child_id;
			int main3_status;
			main3_child_id = fork();

			if (main3_child_id < 0) exit(EXIT_FAILURE);

			//Run 3.c, 3.d, 3.e
			if(main3_child_id == 0) {tiga_c(); tiga_d(); tiga_e();}

			else{
				while((wait(&main3_status)) > 0);
			}
		}
    }
}
```

### 3.a
**Deskripsi**\
Membuat folder bernama `darat` dan 3 detik kemudian membuat folder `air`. Kedua folder dibuat di dalam folder modul2

**Pembahasan**
- Fungsi `tiga_a`
```
void tiga_a(){
	pid_t child_id;
	int status;
	child_id = fork();

	if (child_id < 0) exit(EXIT_FAILURE);
	if (child_id == 0) mkdir_darat();		//Membuat folder darat
	else {
		while((wait(&status)) > 0);
		sleep(3);				//Menunggu 3 detik
		mkdir_air();				//Membuat folder air
	}
}
```
Fungsi `tiga_a` akan memanggil fungsi `mkdir_darat` untuk membuat folder `darat`. Dengan menggunakan perintah `sleep(3)` kita dapat mengatur agar program tidak melakukan apapun selama 3 detik. Setelah itu, panggil program `mkdir_air` untuk membuat folder `air`

- Fungsi `mkdir_darat`
```
void mkdir_darat(){
	char *argv[] = {"mkdir", "-p", "darat", NULL};
	execv("/usr/bin/mkdir", argv);
}
```
Variabel `argv` akan menyimpan perintah mkdir. Perintah tersebut kemudian dijalankan menggunakan `execv`. 

- Fungsi `mkdir_air`
```
void mkdir_air(){
	char *argv[] = {"mkdir", "-p", "air", NULL};
	execv("/usr/bin/mkdir", argv);
}
```
Variabel `argv` akan menyimpan perintah mkdir. Perintah tersebut kemudian dijalankan menggunakan `execv`. 

### 3.b
**Deskripsi**\
Mengunzip file `animal.zip` di dalam folder `modul2`

**Pembahasan**
- Fungsi `tiga_b`
```
void tiga_b(){
	//printf("\nMasuk soal 3b\n");
	char *argv[] = {"unzip", "animal.zip", NULL};
	execv("/usr/bin/unzip", argv);			//Meng-unzip file animal.zip
}
```
Variabel `argv` akan menyimmpan perintah unzip. Perintah tersebut kemudian dijalankan menggunakan `execv`. Hasil unzip akan berada di dalam folder bernama `animal` secara default (pada soal kali ini isinya adalah foto)

### 3.c
**Deskripsi**\
Menggrupkan foto yang berada di folder `animal` berdasarkan format namanya. Foto yang memiliki kata `darat` akan dimasukkan ke dalam folder `darat` dan foto yang memiliki kata `air` akan dimasukkanke dalam folder `air`.

**Pembahasan**
- Fungsi tiga_c
```
void tiga_c(){
        grouping_animal();
        mv_darat(1);
        mv_air(1);
}
```
Digunakan untuk memanggil fungsi `grouping_animal`, `mv_darat`, dan `mv_air`. Fungsi ini hanya dibuat agar proses debugging pada saat pengerjaan lebih mudah.

- Fungsi `grouping_animal`
  - Untuk setiap foto yang ada di dalam folder animal:
    - Nama foto akan disimpan/disalin ke dalam variabel `animal_name`.
    - Mengecek apakah foto mengandung kata `darat`, `air`, dan/atau `bird`. Pengecekan kata `bird` dilakukan untuk mempermudah pengerjaan soal 3d. Jika foto mengandung kata `darat`, maka nama foto tersebut disalin ke dalam variabel `darat_name`. Untuk foto dengan kata `air` akan disimpan ke dalam variabel `air_name` dan `bird` disimpan pada variabel `bird_name`.
    - Jumlah nama foto yang tersimpan di dalam variabel `darat_name`, `air_name`, atau `bird_name` berturut-turut dihitung dengan menambah variabel `darat`, `air`, atau `bird` dengan satu untuk setiap nama baru yang ditambahkan.
```
void grouping_animal(){
        DIR *dp;
        struct dirent *ls;
        dp = opendir(path_animal);
        int i = 0;
        while ((ls = readdir(dp)) != NULL) {
                strcpy(animal_name, ls->d_name);
                if(strstr(animal_name, "darat")){
                        strcpy(darat_name[darat], animal_name);
                        //printf("Darat: %s\n", darat_name[darat]);
                        darat++;
                }
                if(strstr(animal_name, "air")){
                        strcpy(air_name[air], animal_name);
                        //printf("Air: %s\n", air_name[air]);
                        air++;
                }
                if(strstr(animal_name, "bird")){
                        strcpy(bird_name[bird], animal_name);
                        //printf("Bird: %s\n", bird_name[bird]);
                        bird++;
                }
        }
        closedir(dp);
}
```


- Fungsi `mv_darat`
	- Fungsi ini digunakan untuk memindahkan foto di dalam folder `animal` yang namanya terdaftar di dalam variabel `darat_name` ke dalam folder `darat` yang telah dibuat pada [soal 3.a](#3a). 
	- Fungsi ini meminta jumlah foto yang sudah/akan dipindahkan ke dalam folder `darat`. Pada awal fungsi ini di panggil pada fungsi `tiga_c`, nilai yang diberikan adalah 1. Karena baru 1 foto yang akan dipindahkan ke dalam folder `darat`
	- Jika selisih banyaknya foto dan foto yang sudah/akan dipindahkan lebih besar daripada 0, maka fungsi `mv_darat` akan dipanggil kembali.
	- Perintah `mv` dijalankan dengan memanfaatkan `execv`
```
void mv_darat(int total){
	//printf("%s\n", darat_name[darat]);
	pid_t mv_darat_id;
	int mv_darat_status;
	mv_darat_id = fork();
	if(mv_darat_id < 0) exit(EXIT_FAILURE);
	else if(mv_darat_id == 0){
		if(darat-total > 0)	mv_darat(total+1);
		char path_pict[100];
		strcpy(path_pict, path_animal);
		strcat(path_pict, darat_name[darat-total]);
		//printf("%s\n", path_pict);
		char *argv[] = {"mv", path_pict, path_darat, NULL};
		execv("/usr/bin/mv", argv);
	}
	else while((wait(&mv_darat_status)) > 0);
}
```


- Fungsi `mv_air`
	- Fungsi ini digunakan untuk memindahkan foto di dalam folder `animal` yang namanya terdaftar di dalam variabel `air_name` ke dalam folder `air` yang telah dibuat pada [soal 3.a](#3a). 
	- Fungsi ini meminta jumlah foto yang sudah/akan dipindahkan ke dalam folder `air`. Pada awal fungsi ini di panggil pada fungsi `tiga_c`, nilai yang diberikan adalah 1. Karena baru 1 foto yang akan dipindahkan ke dalam folder `air`
	- Jika selisih banyaknya foto dan foto yang sudah/akan dipindahkan lebih besar daripada 0, maka fungsi `mv_air` akan dipanggil kembali.
	- Perintah `mv` dijalankan dengan memanfaatkan `execv`
```
void mv_air(int total){
        //printf("%s\n", air_name[darat]);
        pid_t mv_air_id;
        int mv_air_status;
        mv_air_id = fork();
        if(mv_air_id < 0) exit(EXIT_FAILURE);
        else if(mv_air_id == 0){
                if(air-total > 0)   mv_air(total+1);
                char path_pict[100];
                strcpy(path_pict, path_animal);
                strcat(path_pict, air_name[air-total]);
                //printf("%s\n", path_pict);
                char *argv[] = {"mv", path_pict, path_air, NULL};
                execv("/usr/bin/mv", argv);
        }
        else while((wait(&mv_air_status)) > 0);
}
```

### 3.d
**Deskripsi**\
Menghapus foto di dalam folder `darat` yang mengandung kata `bird` pada namanya.

**Pembahasan**
- Fungsi `tiga_d`
```
void tiga_d(){
	rm_bird(1);
}
```
Digunakan untuk memanggil fungsi `rm_bird`. Fungsi ini hanya dibuat agar proses debugging pada saat pengerjaan lebih mudah.


- Fungsi `rm_bird`
  - Fungsi ini digunakan untuk menghapus foto di dalam folder `darat` yang namanya terdaftar di dalam variabel `bird_name`.
	- Fungsi ini meminta jumlah foto yang sudah/akan dihapus di dalam folder `darat`. Pada awal fungsi ini di panggil pada fungsi `tiga_d`, nilai yang diberikan adalah 1. Karena baru 1 foto yang akan dihapus di dalam folder `darat`
	- Jika selisih banyaknya foto dan foto yang sudah/akan dihapus lebih besar daripada 0, maka fungsi `rm_bird` akan dipanggil kembali.
	- Perintah `rm` dijalankan dengan memanfaatkan `execv`
```
void rm_bird(int total){
        pid_t rm_bird_id;
        int rm_bird_status;
        rm_bird_id = fork();
        if(rm_bird_id < 0) exit(EXIT_FAILURE);
        else if(rm_bird_id == 0){
                if(bird-total > 0)   rm_bird(total+1);
                char path_pict[100];
                strcpy(path_pict, path_darat);
                strcat(path_pict, bird_name[bird-total]);
                //printf("%d %s\n", bird, path_pict);
                char *argv[] = {"rm", path_pict, NULL};
                execv("/usr/bin/rm", argv);
        }
        else while((wait(&rm_bird_status)) > 0);
}
```

### 3.e
**Deskripsi**\
Membuat list bernama `list.txt` yang berisi nama foto-foto yang berada di dalam folder `air` dengan format UID_FILE Permission_Nama File.jpg/png

**Pembahasan**
```
void tiga_e(){
	FILE* fp;
	fp = fopen("/home/graidy/modul2/air/list.txt", "a");
	int result;
	uid_t uid = getuid();
	for(int j = 0; j < air; j++){
		fprintf(fp, "%d_", uid);
		result = access ("/home/graidy/modul2/air/list.txt", R_OK);
		if(result == 0){
			fprintf(fp, "r");
		}
		result = access ("/home/graidy/modul2/air/list.txt", W_OK);
		if(result == 0){
			fprintf(fp, "w");
		}
		result = access ("/home/graidy/modul2/air/list.txt", X_OK);
		if(result == 0){
			fprintf(fp, "x");
		}
		fprintf(fp, "_%s\n", air_name[j]);
	}
}
```
- Perintah `fp = fopen("/home/graidy/modul2/air/list.txt", "a");` digunakan untuk membuka file bernama `list.txt`. Jika file tersebut belum ada, maka akan otomatis dibuat dengan nama tersebut.
- Perintah `uid_t uid = getuid();` digunakan untuk mendapatkan UID. Kemudian, uid tersebut ditambilkan dengan menggunakan perintah `fprintf(fp, "%d_", uid);`
- Variabel result akan menyimpan nilai yang menandakan ada tidaknya izin untuk `read`, `write`, atau `execute` pada file foto.
- Perintah `access ("/home/graidy/modul2/air/list.txt", R_OK);` digunakan untuk mengecek izin `read`.
	- Jika terdapat izinnya, maka akan menampilkan `r`
- Perintah `access ("/home/graidy/modul2/air/list.txt", W_OK);` digunakan untuk mengecek izin `write`.
	- Jika terdapat izinnya, maka akan menampilkan `w`
- Perintah `access ("/home/graidy/modul2/air/list.txt", X_OK);` digunakan untuk mengecek izin `execute`
	- Jika terdapat izinnya, maka akan menampilkan `x`
- Setelah izin dicek, maka nama file ditampilkan agar sesuai dengan format yang diminta soal.

